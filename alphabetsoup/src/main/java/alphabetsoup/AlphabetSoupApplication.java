package alphabetsoup;

import java.io.IOException;

/**
 * Main class for Alphabet Soup project
 * 
 * @author ZCarmines
 */
public class AlphabetSoupApplication 
{
    public static void main(String[] args) {
        
        AnswerKeyProcessor processor = new AnswerKeyProcessor();
        try {
            processor.generateAnswerKey();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

