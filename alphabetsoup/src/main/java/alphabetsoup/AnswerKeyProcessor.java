package alphabetsoup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class will take word search metadata and output an answer key to the console
 * @author ZCarmines
 */
public class AnswerKeyProcessor {

    public void generateAnswerKey() throws IOException {

        System.out.println("Enter a filename");
        Scanner input = new Scanner(System.in);    
        String fileName = input.nextLine();
        FileReader reader;

        try {
            reader = new FileReader(fileName);
            BufferedReader br = new BufferedReader(reader);
            try {
                //Determine dimensions of puzzle
                String firstLine = br.readLine();
                String[] dimensionStrings = firstLine.split("x");
                int dimension = Integer.parseInt(dimensionStrings[0]);
                String[][] puzzle = new String[dimension][dimension];

                //Create puzzle array
                String nextLine;
                String[] currentLine;
                for (int x = 0; x < dimension; x++) {
                nextLine = br.readLine();
                currentLine = nextLine.split(" ");
                    for (int y = 0; y < dimension; y++) {
                        puzzle[x][y] = currentLine[y];
                    }
                }

                //Store list of hidden words
                List<String> hiddenWords = new ArrayList<String>();
                String wordLine = br.readLine();
                while (wordLine != null) {
                    hiddenWords.add(wordLine);
                    wordLine = br.readLine();
                }

                //Generate output
                for (String currentWord : hiddenWords) {
                String originalWord = currentWord;
                currentWord = currentWord.replace(" ","");
                Word word = new Word();
                word = generateOutput(currentWord, puzzle, dimension);
                System.out.println(originalWord + " " + word.getStartY() + ":" + word.getStartX() + " " + word.getEndY() + ":" + word.getEndX());
                }
            }
            catch(IOException e) {
                System.out.println("File content cannot be read");
                e.printStackTrace();
            }
            try {
                reader.close();
            } catch (IOException e) {
                System.out.println("Error closing file reader");
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Filename cannot be read");
            e.printStackTrace();
        } 
         input.close();
    }

    private Word generateOutput(String currentWord, String[][] puzzle, int dimension) {

        char[] currentWordChars;
        int currentWordCharIndex = 0;
        int startX = 0;
        int startY = 0;
        int endX = 0;
        int endY = 0;

        currentWordCharIndex=0;
        currentWordChars = currentWord.toCharArray();

            //outer loop, look through each position for starting character
            for (int x=0; x<dimension; x++) {

                for (int y=0; y<dimension; y++) {

                    if (puzzle[y][x].contains(String.valueOf(currentWordChars[currentWordCharIndex]))) {

                        //first character found, save starting point and call recursive function with coordinates
                        startX = x;
                        startY = y;
                        Index endIndex = findWordFromStartPosition(currentWord, puzzle, startX, startY, 0, dimension, null); 
                        if (endIndex != null) {
                            endX = endIndex.getXIndex();
                            endY = endIndex.getYIndex();

                            Word word = new Word();
                            word.setStartX(startX);
                            word.setStartY(startY);
                            word.setEndX(endX);
                            word.setEndY(endY);
                            return word;
                        }
                    }  
                }
            }
            return null;
    }

    public Index findWordFromStartPosition(String currentWord, String[][] puzzle, int x, int y, int currentIndex, int dimension, String direction) {

        //word completed, return end index
        if (currentIndex==currentWord.length()-1) {
            Index endIndex = new Index();
            endIndex.setXIndex(x);
            endIndex.setYIndex(y);
            return endIndex;
        }

        char currentChar = currentWord.charAt(currentIndex);

        if (x < 0 || x >= dimension || y < 0 || y >= dimension || !puzzle[y][x].contains(String.valueOf(currentChar))) {
            return null;
        }
        Index nextIndex = null;
        //check top left
        if (!(x-1<0 || y-1<0) && !(currentIndex>0 && direction!="topLeft")) {
        nextIndex = findWordFromStartPosition(currentWord, puzzle, x-1, y-1, currentIndex+1, dimension, "topLeft");
        }
        //check top
        if ((nextIndex==null && !(y-1<0)) && !(currentIndex>0 && direction!="top")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x, y-1, currentIndex+1, dimension, "top");
        }
        //check top right
        if ((nextIndex==null && !(x-1<0 || x+1>dimension)) && !(currentIndex>0 && direction!="topRight")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x+1, y-1, currentIndex+1, dimension, "topRight");
        }
        //check left
        if ((nextIndex==null && !(x-1<0)) && !(currentIndex>0 && direction!="left")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x-1, y, currentIndex+1, dimension, "left");
        }
        //check right
        if ((nextIndex==null && !(x+1>dimension)) && !(currentIndex>0 && direction!="right")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x+1, y, currentIndex+1, dimension, "right");
        }
        //check bottom left
        if ((nextIndex==null && !(x-1<0 || y+1>dimension)) && !(currentIndex>0 && direction!="bottomLeft")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x-1, y+1, currentIndex+1, dimension, "bottomLeft");
        }
        //check bottom
        if ((nextIndex==null && !(y+1>dimension)) && !(currentIndex>0 && direction!="bottom")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x, y+1, currentIndex+1, dimension, "bottom");
        }
        //check bottom right
        if ((nextIndex==null && !(x+1>dimension || y+1>dimension)) && !(currentIndex>0 && direction!="bottomRight")) {
            nextIndex = findWordFromStartPosition(currentWord, puzzle, x+1, y+1, currentIndex+1, dimension, "bottomRight");
        }
        return nextIndex;
     }

}
