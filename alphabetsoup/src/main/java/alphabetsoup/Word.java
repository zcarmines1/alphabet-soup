package alphabetsoup;

import lombok.Data;

/**
 * Class to represent a hidden Word object
 * @author ZCarmines
 */
@Data
public class Word {

    private int startX;
    private int startY;
    private int endX;
    private int endY;    
}
