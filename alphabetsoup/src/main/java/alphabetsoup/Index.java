package alphabetsoup;

import lombok.Data;

/**
 * Class to represent an Index object
 * @author ZCarmines
 */
@Data
public class Index {

    private int xIndex;
    private int yIndex;
    
}
